package com.vector.demo.git.app;

import java.util.logging.Logger;

import com.vector.demo.git.app.model.DummyVisualizer;
import com.vector.demo.git.app.model.IVisualizer;
import com.vector.demo.git.app.model.MainArgsMessage;
/**
 * Demo 
 * @author max
 *
 */
public class Demo {

	public static final String HELLO_DEFA = "Hello GIT!!!";

	private static final Logger fLogger=Logger.getLogger(Demo.class.getPackage().getName());
	public static void main(String[] args) {
		fLogger.info("Iniciando aplicación de Demo");
		IVisualizer v = new DummyVisualizer();
		v.display(new MainArgsMessage(args));
		fLogger.info("Bye Bye!!");
	}

}
