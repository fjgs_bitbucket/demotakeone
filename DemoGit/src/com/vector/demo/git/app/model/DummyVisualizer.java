package com.vector.demo.git.app.model;
/**
 * DummyVisualizer
 * @author max
 *
 */
public class DummyVisualizer implements IVisualizer {

	@Override
	public boolean display(IMessage message) {
		System.out.println(message.getMessage());
		return true;
	}

}
