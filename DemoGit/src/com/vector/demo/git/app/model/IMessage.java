package com.vector.demo.git.app.model;
/**
 * IMessage - Interfaz del mensaje 
 * @author fjgonzalezs
 *
 */
public interface IMessage {
	public String getMessage();
}
