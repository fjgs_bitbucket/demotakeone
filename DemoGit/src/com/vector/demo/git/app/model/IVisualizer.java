package com.vector.demo.git.app.model;
/**
 * IVisualizer - Interfaz del visualizador
 * @author max
 *
 */
public interface IVisualizer {
	public boolean display(IMessage message);
}
