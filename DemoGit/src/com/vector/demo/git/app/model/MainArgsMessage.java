package com.vector.demo.git.app.model;

import com.vector.demo.git.app.Demo;

public class MainArgsMessage implements IMessage {
	public String[] args = null;
	public MainArgsMessage(String[] args) {
		super();
		this.args = args;
	}
	@Override
	public String getMessage() {
		String message = null;
		if(args!=null && args.length>0){
			message = "Hello "+args[0]+" !!!";
		}
		return message!=null?message:Demo.HELLO_DEFA;
	
}

}
